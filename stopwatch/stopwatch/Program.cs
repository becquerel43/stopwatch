﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;


//program should give time in seconds, minutes, hours, days
//should save time on program exit and resume when opening. if file is missing, reset

namespace stopwatch
{
    class Program
    {

        static Stopwatch stopwatch = new Stopwatch();

        static void Main(string[] args)
        {
            stopwatch.Start();

            while (true)
            {
                Console.WriteLine("Select action:\r\n1) Check time\r\n2) Close program"); ;

                int choice = Int32.Parse(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        getTime();
                        break;
                    case 2:
                        System.IO.File.WriteAllText("../../time.txt", stopwatch.ElapsedMilliseconds.ToString());
                        Environment.Exit(0);
                        break;
                    default:

                        Console.WriteLine("Failure, try again");
                        break;
                }
            }
        }

        static void getTime()
        {

            long seconds = stopwatch.ElapsedMilliseconds / 1000;
            long minutes = stopwatch.ElapsedMilliseconds / 60000;
            long hours = stopwatch.ElapsedMilliseconds / 3600000;
            long days = stopwatch.ElapsedMilliseconds / 43200000;

            Console.Clear();
            Console.WriteLine(
                "{0} seconds, {1} minutes, {2} hours, {3} days.\r\n", seconds, minutes, hours, days);
        }
    }


}